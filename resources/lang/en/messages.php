<?php
declare(strict_types=1);

return [
    "command_useradd_description" => "Add a new Laravel user.",
    "command_useradd_question_name" => "Enter user name",
    "command_useradd_question_email" => "Enter user email",
    "command_useradd_question_random_password" => "Do you wish to create a random password?",
];
