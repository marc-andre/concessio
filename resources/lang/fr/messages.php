<?php
declare(strict_types=1);

return [
    'exception_message' => 'Message de l\'exception : :message',
];
