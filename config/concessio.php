<?php


return [
    // The class name of the user model to be used.
    "model"      => "App\User",

    // Validation rules to check against the input values.
    "validation" => [
        "rule" => [
            "name"     => "string|max:255",
            "email"    => "string|email|max:255|unique:users",
            "password" => "string|min:6",
        ],
    ],

];
