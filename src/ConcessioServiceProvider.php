<?php
declare(strict_types=1);

namespace MarcAndreAppel\Concessio;

use Illuminate\Support\ServiceProvider;

class ConcessioServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes(
            [
                __DIR__.'/../config/concessio.php' => config_path('concessio.php'),
            ]
        );
        $this->publishes(
            [
                __DIR__.'/../resources/lang' => "{$this->app['path.lang']}/vendor/concessio",
            ]
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/Config/createuser.php',
            'createuser'
        );

        $this->commands(
            [
                Commands\UsersList::class,
                Commands\CreateUser::class,
                Commands\UserRemove::class,
            ]
        );
    }
}
