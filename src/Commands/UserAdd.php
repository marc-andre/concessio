<?php
declare(strict_types=1);

namespace MarcAndreAppel\Concessio\Commands;

use Illuminate\Console\Command;
use MarcAndreAppel\Concessio\Notifications\UserAccountCreated as UserAccountCreatedNotification;
use Validator;

class UserAdd extends Command
{
    /** @var string */
    protected $signature = 'user:add';

    public function __construct()
    {
        $this->description = __("commands.user.add.description");

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $name  = $this->ask(
            __("concessio::messages.command_useradd_question_name"),
            [
                "name" => config("concessio.validation.rule.name"),
            ]
        );
        $email = $this->extendedQuestion(
            __("concessio::messages.command_useradd_question_email"),
            [
                "email" => config("concessio.validation.rule.email"),
            ]
        );

        if ($this->confirm("Do you wish to create a random password?")) {
            $password = str_random(8);
            $this->info('*The randomly created password is: '.$password);
        } else {
            $password = $this->validate_ask(
                'Enter user password',
                ['password' => config('createuser.validation_rules.password')]
            );
        }

        $model = config('createuser.model');

        $user = new $model();

        $user->name     = $name;
        $user->email    = $email;
        $user->password = bcrypt($password);

        $user->save();

        $this->info('New user created!');

        if ($this->confirm('Do you wish to send the user a notification with their credentials?')) {
            $user->notify(new UserAccountCreatedNotification($password));
            $this->info('Email sent.');
        }
    }

    /**
     * @param  string  $question
     * @param  string  $rules
     * @param  string|null  $default
     *
     * @return mixed
     */
    public function ask(string $question, string $rules, string $default = null)
    {
        $value = $this->output->ask($question, $default);

        $validate = $this->validateInput($rules, $value);

        if ($validate !== true) {
            $this->error($validate);
            $value = $this->ask($question, $rules);
        }

        return $value;
    }

    public function validateInput($rules, $value)
    {
        $validator = Validator::make([key($rules) => $value], $rules);

        if ($validator->fails()) {
            return $error = $validator->errors()->first(key($rules));
        }

        return true;
    }
}
