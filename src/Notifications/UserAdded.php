<?php
declare(strict_types=1);

namespace MarcAndreAppel\Concessio\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserAdded extends Notification
{

    /** @var string     */
    public $password;

    /**
     * @param  string  $password
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->success()
            ->greeting('Welcome!')
            ->line('A user account has been created for you with the following information:')
            ->line('Your name: **'.$notifiable->name.'**')
            ->line('Your email address: **'.$notifiable->email.'**')
            ->line('Your password: **'.$this->password.'**')
            ->action('Start here!', config('app.url'))
            ->line('Enjoy using our application!');
    }

}
