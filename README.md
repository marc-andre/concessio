# Concessio

Roles made easy for Laravel.

## Installation

Require the package with composer:
```sh
composer require marc-andre/concessio
```

## Important notes

The package is incompatible with `boaideas/laravel-cli-create-user`, because it is loosely based upon it.